package com.sph.sg.asignment

import android.view.View
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import com.sph.sg.asignment.adapter.RecordModel
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.TypeSafeMatcher
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

import androidx.test.espresso.Espresso.onData
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.RootMatchers.withDecorView
import androidx.test.espresso.matcher.ViewMatchers.*
import org.hamcrest.Matchers.not
import org.hamcrest.core.Is.`is`

@RunWith(AndroidJUnit4::class)
@LargeTest
class MainActivityTest {

    @Rule
    var activityActivityTestRule = ActivityTestRule(MainActivity::class.java, false, false)

    @Test
    fun checkRecyclerViewsVisibility() {
        // verify the visibility of recycler view on screen
        onView(withId(R.id.record_list_rv)).check(matches(isDisplayed()))
    }

    @Test
    fun checkProgressBarVisibility() {
        // verify the visibility of progressbar on screen
        onView(withId(R.id.progressBar)).check(matches(isDisplayed()))
    }

    @Test
    fun checkNoDataTextVisibility() {
        // verify the visibility of no data TextView on screen By default it should be GONE
        onView(withId(R.id.no_data_tv)).check(matches(isDisplayed()))
    }

    @Test
    fun checkInternet() {
        val isAvailable = activityActivityTestRule.activity.mNetworkMonitor.isConnected
        if (isAvailable) {
            onView(withText("Internet is Available")).inRoot(withDecorView(not<View>(`is`<View>(activityActivityTestRule.activity.window.decorView))))
                .check(matches(isDisplayed()))
        } else {
            onView(withText("No Internet")).inRoot(withDecorView(not<View>(`is`<View>(activityActivityTestRule.activity.window.decorView))))
                .check(matches(isDisplayed()))
        }
    }

    @Test
    fun clickItemIv() {
        onData(isLowData(true))
            .inAdapterView(withId(R.id.data_usage_low_iv))
            .perform(click())
    }

    @Test
    fun checkRvItemIvVisibilityTrue() {
        onData(isLowData(true))
            .inAdapterView(withId(R.id.data_usage_low_iv))
            .check(matches(isDisplayed()))
    }

    @Test
    fun checkRvItemIvVisibilityFalse() {
        onData(isLowData(false))
            .inAdapterView(withId(R.id.data_usage_low_iv))
            .check(matches(isDisplayed()))
    }


    private fun isLowData(visibility: Boolean): Matcher<RecordModel> {
        return object : TypeSafeMatcher<RecordModel>() {
            override fun matchesSafely(recordModel: RecordModel): Boolean {
                return recordModel.isLowData.get() == visibility
            }

            override fun describeTo(description: Description) {
                description.appendText("Image view visibility: $visibility")
            }
        }
    }
}
