package com.sph.sg.asignment.model

import com.fasterxml.jackson.annotation.JsonProperty
import java.math.BigDecimal

/**
 * Class which provides a model for post
 * @constructor Sets all properties of the post
 * @property _id the unique identifier of the post
 * @property volume_of_mobile_data the title of the post
 * @property quarter the content of the post
 */
data class RecordsItem(
    @JsonProperty("_id")
    val _id: Int,
    @JsonProperty("volume_of_mobile_data")
    val volume_of_mobile_data: Double,
    @JsonProperty("quarter")
    val quarter: String
)