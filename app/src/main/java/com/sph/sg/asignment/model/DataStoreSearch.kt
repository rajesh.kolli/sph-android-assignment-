package com.sph.sg.asignment.model

import com.fasterxml.jackson.annotation.JsonProperty

class DataStoreSearch {
    @JsonProperty("result")
    val result: Result? = null
    @JsonProperty("help")
    val help: String? = null
    @JsonProperty("success")
    val success: Boolean? = null
}