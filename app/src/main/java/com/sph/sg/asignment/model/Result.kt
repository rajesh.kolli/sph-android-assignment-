package com.sph.sg.asignment.model

import com.fasterxml.jackson.annotation.JsonProperty

class Result {
    @JsonProperty("total")
    val total: Int? = null
    @JsonProperty("records")
    val records: List<RecordsItem>? = null
    @JsonProperty("_links")
    val _links: Links? = null
    @JsonProperty("limit")
    val limit: Int? = null
    @JsonProperty("resource_id")
    val resource_id: String? = null
    @JsonProperty("fields")
    val fields: List<FieldsItem>? = null
}