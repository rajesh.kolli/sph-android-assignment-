package com.sph.sg.asignment.dagger;

import android.content.Context;
import com.sph.sg.asignment.MainApplication;
import com.sph.sg.asignment.api.NetworkServiceGenerator;
import com.sph.sg.asignment.api.RestApi;
import com.sph.sg.asignment.bus.MainThreadBus;
import com.sph.sg.asignment.utils.NetworkMonitor;
import com.squareup.otto.Bus;
import dagger.Module;
import dagger.Provides;

import javax.inject.Singleton;

@Module
public class MainModule {

    @Provides
    @Singleton
    Context provideContext() {
        return MainApplication.instance.getApplicationContext();
    }

    @Provides
    @Singleton
    RestApi provideApi() {
        return NetworkServiceGenerator.INSTANCE.createService(RestApi.class);
    }

    @Provides
    @Singleton
    Bus provideBus() {
        return new MainThreadBus();
    }

    @Provides
    @Singleton
    NetworkMonitor provideNetworkMonitor() {
        return new NetworkMonitor();
    }

}
