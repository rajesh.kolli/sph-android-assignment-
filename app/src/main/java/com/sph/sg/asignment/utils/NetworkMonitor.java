package com.sph.sg.asignment.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.sph.sg.asignment.MainApplication;

import javax.inject.Inject;

public class NetworkMonitor {

    @Inject
    Context context;

    @Inject
    public NetworkMonitor() {
        MainApplication.instance.getMainComponent().inject(this);
    }

    public boolean isConnected() {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
