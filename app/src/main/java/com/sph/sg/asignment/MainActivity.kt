package com.sph.sg.asignment

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.sph.sg.asignment.adapter.RecordModel
import com.sph.sg.asignment.adapter.RecordsListAdapter
import com.sph.sg.asignment.api.RestApi
import com.sph.sg.asignment.model.DataStoreSearch
import com.sph.sg.asignment.utils.NetworkMonitor
import com.squareup.otto.Bus
import kotlinx.android.synthetic.main.activity_main.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    lateinit var recordsListItems: MutableList<RecordModel>

    lateinit var adapter: RecordsListAdapter

    @Inject
    lateinit var mRestApi: RestApi

    @Inject
    lateinit var mBus: Bus

    @Inject
    lateinit var mNetworkMonitor: NetworkMonitor

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        MainApplication.instance.mainComponent?.inject(this)

        no_data_tv.visibility = View.GONE
        progressBar.visibility = View.VISIBLE


        recordsListItems = ArrayList<RecordModel>()

        adapter = RecordsListAdapter(this, recordsListItems)
        val layoutManager = LinearLayoutManager(this)
        layoutManager.orientation = RecyclerView.VERTICAL
        record_list_rv.layoutManager = layoutManager
        record_list_rv.setHasFixedSize(true)
        record_list_rv.adapter = adapter

        if (checkInternet())
            onDataGet()
    }

    fun checkInternet(): Boolean {
        if (!mNetworkMonitor.isConnected) {
            showDialog("Oops! No internet connection, please enable daa or wifi and try again.", "Try Again", false)
            return false
        }
        return true
    }

    private var mDialog: AlertDialog? = null
    public fun showDialog(message: String, buttonText: String, cancelable: Boolean) {
        if (mDialog != null && mDialog!!.isShowing)
            mDialog!!.dismiss()

        val builder = AlertDialog.Builder(this)
        builder.setMessage(message)
        builder.setCancelable(cancelable)
        builder.setPositiveButton(buttonText) { _, _ -> onDataGet() }

        mDialog = builder.create()
        mDialog!!.show()
    }

    private fun onDataGet() {

        val call = mRestApi.getDataStore()
        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                try {
                    val mapper = ObjectMapper()
                    mapper.enable(DeserializationFeature.ACCEPT_EMPTY_ARRAY_AS_NULL_OBJECT)
                    val dataStoreSearch = mapper.readValue(response.body()!!.bytes(), DataStoreSearch::class.java)
                    loadData(dataStoreSearch)

                } catch (e: Exception) {
                    e.printStackTrace()
                    showDialog(e.localizedMessage, getString(android.R.string.ok), true)
                    progressBar.visibility = View.GONE
                    no_data_tv.visibility = View.VISIBLE

                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                showDialog(t.localizedMessage, getString(android.R.string.ok), true)
                println()
                progressBar.visibility = View.GONE
                no_data_tv.visibility = View.VISIBLE
            }
        })
    }

    private fun loadData(dataStoreSearch: DataStoreSearch?) {
        if (dataStoreSearch?.result?.records != null) {
            val recordsItems = dataStoreSearch.result.records

            Collections.sort(recordsItems) { (_, _, quarter),
                                             (_, _, quarter2)
                ->
                quarter.compareTo(quarter2)
            }

            var previousData = 0.0
            var previousYear = ""
            var recordModel = RecordModel()
            var isLowData = false
            var lowDataMessage = ""

            for (i in recordsItems.indices) {
                val record = recordsItems[i]
                val quarter = record.quarter
                val year = splitQuarter(quarter)
                val data = record.volume_of_mobile_data

                if (previousYear.isNotEmpty() && previousYear != year) {
                    recordsListItems.add(recordModel)
                    previousData = 0.0
                    isLowData = false
                    lowDataMessage = ""
                }

                recordModel = RecordModel()
                recordModel.year.set(year)
                if (!isLowData) {
                    isLowData = previousData > 0 && data < previousData
                    lowDataMessage = quarter + "'s data is low \nData Used: " + String.format("%.6f", data)
                }

                recordModel.isLowData.set(isLowData)
                recordModel.lowDataMessage.set(lowDataMessage)
                previousData += data
                recordModel.dataUsed.set(String.format("%.6f", previousData))


                previousYear = year
                if (i == recordsItems.size - 1) {
                    recordsListItems.add(recordModel)
                }
            }
        }

        if (recordsListItems!!.isNotEmpty()) {
            progressBar.visibility = View.GONE
            no_data_tv.visibility = View.GONE
        } else {
            progressBar.visibility = View.GONE
            no_data_tv.visibility = View.VISIBLE
        }

        adapter.setRecordsLists(recordsListItems)
    }

    private fun splitQuarter(quarter: String): String {
        val splitQuarter = quarter.split("-".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        return if (splitQuarter.isNotEmpty()) {
            splitQuarter[0]
        } else quarter
    }

    override fun onStart() {
        super.onStart()
        mBus.register(this)
    }

    override fun onStop() {
        super.onStop()
        mBus.unregister(this)
    }
}