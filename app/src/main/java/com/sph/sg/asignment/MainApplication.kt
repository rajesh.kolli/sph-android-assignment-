package com.sph.sg.asignment

import android.app.Application
import com.sph.sg.asignment.dagger.MainComponent
import com.sph.sg.asignment.dagger.DaggerMainComponent

class MainApplication : Application() {

    var mainComponent: MainComponent? = null
        private set


    override fun onCreate() {
        instance = this
        super.onCreate()
        BASE_URL = getString(R.string.base_url)
        mainComponent = DaggerMainComponent.builder().build()

    }

    companion object {
        lateinit var instance: MainApplication
            internal set

        var BASE_URL = ""
    }
}
