package com.sph.sg.asignment.dagger;


import com.sph.sg.asignment.MainActivity;
import com.sph.sg.asignment.utils.NetworkMonitor;
import dagger.Component;

import javax.inject.Singleton;

@Component(modules = {MainModule.class})
@Singleton
public interface MainComponent {

    void inject(MainActivity mainActivity);

    void inject(NetworkMonitor networkMonitor);
}
