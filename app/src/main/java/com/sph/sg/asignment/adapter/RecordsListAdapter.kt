package com.sph.sg.asignment.adapter

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.sph.sg.asignment.R
import com.sph.sg.asignment.databinding.LayoutRecordsRowBinding
import kotlinx.android.synthetic.main.layout_records_row.view.*

public class RecordsListAdapter(context: Context, var recordsList: MutableList<RecordModel>?) : RecyclerView.Adapter<RecordsListAdapter.ViewHolder>() {
    private val inflater: LayoutInflater = LayoutInflater.from(context)
    var mBindingViews = HashMap<Int, RecyclerView.ViewHolder>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = inflater.inflate(R.layout.layout_records_row, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        mBindingViews[position] = holder
        val item = recordsList!![position]
        holder.takeItem(item)
    }


    override fun getItemCount(): Int {
        return if (recordsList != null) {
            recordsList!!.size
        } else {
            1
        }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var binding: ViewDataBinding?
            get() = DataBindingUtil.getBinding(itemView)

        init {

            binding = DataBindingUtil.bind<LayoutRecordsRowBinding>(itemView)

        }


        fun takeItem(item: RecordModel) {
            binding!!.setVariable(com.sph.sg.asignment.BR.recordsItem, item)
            binding!!.executePendingBindings()
            itemView.data_usage_low_iv.setOnClickListener {
                    v -> Toast.makeText(v.context, item.lowDataMessage.get()!! + "", Toast.LENGTH_LONG).show()
            }
        }
    }


    fun getRecordLists(): List<RecordModel>? {
        return recordsList
    }

    fun setRecordsLists(todoLists: MutableList<RecordModel>) {
        this.recordsList = todoLists
        notifyDataSetChanged()
    }

}