package com.sph.sg.asignment.adapter

import androidx.databinding.BaseObservable
import androidx.databinding.ObservableField

class RecordModel : BaseObservable() {
    var year = ObservableField<String>()
    var dataUsed = ObservableField<String>()
    var lowDataMessage = ObservableField<String>()
    var isLowData = ObservableField<Boolean>()
}
