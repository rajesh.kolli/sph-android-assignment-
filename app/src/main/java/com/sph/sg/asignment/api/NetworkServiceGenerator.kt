package com.sph.sg.asignment.api

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.sph.sg.asignment.MainApplication
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory

import java.io.IOException
import java.util.concurrent.TimeUnit

import com.sph.sg.asignment.MainApplication.Companion.BASE_URL

object NetworkServiceGenerator {

    private var httpClient: OkHttpClient? = null

    private val gson = GsonBuilder()
        .setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ")
        .setPrettyPrinting() // Pretty print
        .create()

    init {
        httpClient = OkHttpClient.Builder().addInterceptor { chain ->
            val request = chain.request().newBuilder().build()
            chain.proceed(request)
        }
            .readTimeout(30, TimeUnit.SECONDS)
            .connectTimeout(30, TimeUnit.SECONDS)
            .build()
    }


    fun <S> createService(serviceClass: Class<S>): S {
        val builder = Retrofit.Builder()
            .baseUrl(MainApplication.BASE_URL)
            .addConverterFactory(ScalarsConverterFactory.create())
            .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))

        val retrofit = builder.client(httpClient!!).build()
        return retrofit.create(serviceClass)
    }
}
