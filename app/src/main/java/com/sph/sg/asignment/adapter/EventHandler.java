package com.sph.sg.asignment.adapter;

import android.view.View;
import android.widget.Toast;

public class EventHandler {

    public void onLowDataImageClick(View view, RecordModel recordModel) {
        Toast.makeText(view.getContext(), recordModel.getLowDataMessage().get() + "", Toast.LENGTH_LONG).show();
    }
}
