package com.sph.sg.asignment.model

import com.fasterxml.jackson.annotation.JsonProperty

class FieldsItem {
    @JsonProperty("id")
    val id: String? = null
    @JsonProperty("type")
    val type: String? = null
}