package com.sph.sg.asignment.api

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.GET

interface RestApi {
    /**
     * Get the data store from the API
     */
    @GET("action/datastore_search?resource_id=a807b7ab-6cad-4aa6-87d0-e283a7353a0f")
    fun getDataStore(): Call<ResponseBody>
}