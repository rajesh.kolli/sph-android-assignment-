package com.sph.sg.asignment.model

import com.fasterxml.jackson.annotation.JsonProperty

class Links {
    @JsonProperty("next")
    val next: String? = null
    @JsonProperty("start")
    val start: String? = null
}